from flask import Flask
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)

logger = logging.getLogger(__name__)

log_formatter = logging.Formatter('%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')

my_handler = RotatingFileHandler("example.log", maxBytes=5*1024*1024, backupCount=10)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.ERROR)

logger.addHandler(my_handler)

from app import views