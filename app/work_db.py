import psycopg2
import datetime
from binascii import hexlify
from hashlib import sha256
from os import urandom

import traceback
from app import logger    # logger.error("some text")
from app.config import create_connection


def login_db(di):
    conn = create_connection()
    status = True
    info_user_json={}
    if conn:
        cur = conn.cursor()
        try:
            now = datetime.datetime.now()
            session_key = sha256(bytes(di['login'][0] + str(now), 'utf-8') + hexlify(urandom(22))).hexdigest()

            cur.execute('''
                            SELECT 
                                    login, 
                                    password 
                            FROM users
                            WHERE (
                                    login = '{}' 
                                    AND password = '{}')
                        '''.format(
                                    di['login'][0],
                                    di['pas'][0]))
            row = cur.fetchone()
            if row is not None:
                cur.execute('''
                                UPDATE users 
                                SET session_key = '{}'
                                WHERE (login = '{}' 
                                      and password = '{}')
                            '''.format(session_key,
                                       di['login'][0],
                                       di['pas'][0]))
                conn.commit()
                cur.execute('''
                                SELECT * 
                                FROM users
                                WHERE (
                                        login = '{}' 
                                        AND password = '{}')
                            '''.format(
                                        di['login'][0],
                                        di['pas'][0]))
                user = cur.fetchone()
                info_user_json = {
                    'id': user[0],
                    'name': user[1],
                    'last_name': user[2],
                    'login': user[3],
                    'password': user[4],
                    'email': user[5],
                    'user_status': user[6],
                    'phone': user[7],
                    'session_key': user[8]}
            else:
                status = False
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status, info_user_json


def registration_db(di):
    conn = create_connection()
    status = True
    if di['login'][0] != '' and di['email'][0] != '' and di['pas'][0] != '':
        if conn:
            cur = conn.cursor()
            try:
                cur.execute('''
                                INSERT INTO 
                                users (
                                        name, 
                                        last_name, 
                                        login, 
                                        email, 
                                        phone, 
                                        password, 
                                        user_status
                                        )
                                VALUES ('{}','{}','{}','{}','{}','{}','{}')
                            '''.format(
                                        di['name'][0],
                                        di['lastname'][0],
                                        di['login'][0],
                                        di['email'][0],
                                        di['phone'][0],
                                        di['pas'][0],
                                        2))

                conn.commit()
            except:
                logger.error(traceback.format_exc())
                traceback.print_exc()
                status = False
            cur.close()
            conn.close()
    else:
        status = False
    return status


def add_tour_func(di):
    conn = create_connection()
    status = True
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            INSERT INTO
                             tour (
                                    duration, 
                                    tour_operator, 
                                    description, 
                                    country, 
                                    city
                                  )
                            VALUES ({}, {}, '{}', {}, {})
                        '''.format(
                                    di['dur'][0],
                                    di['tourOp'][0],
                                    di['desc'][0],
                                    di['country'][0],
                                    di['city'][0]
                                ))
            conn.commit()

            key = di.keys()
            cur.execute('''
                            SELECT 
                                  id 
                            FROM tour
                            WHERE (duration = {}, 
                                    tour_operator = {}, 
                                    description = '{}', 
                                    country = {}, 
                                    city = {})
                        '''.format(
                                    di['dur'][0],
                                    di['tourOp'][0],
                                    di['desc'][0],
                                    di['country'][0],
                                    di['city'][0]
                                    ))
            id_tour = cur.fetchone()[0]
            for row in key:
                if row.find('date') != -1:
                    cur.execute('''
                                    INSERT INTO 
                                              tour_date (
                                                        id_tour, 
                                                        id_date
                                                        )
                                    VALUES ('{}', (SELECT 
                                                          id 
                                                    FROM date_
                                                    WHERE 
                                                          date_ = '{}'::date))
                                '''.format(
                                            id_tour,
                                            di[row][0]))
                    conn.commit()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status


def select_tour_func():
    conn = create_connection()
    list_tour_json = []
    status = True
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT 
                                  country.id, 
                                  name_country 
                            FROM tour
                            INNER JOIN country ON tour.country = country.id
                            GROUP BY (country.id
                                      )
                            ORDER BY name_country
                        ''')
            list_tour = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        else:
            list_tour_json = [{
                "id": tour[0],
                "name_country": tour[1]} for tour in list_tour]

        cur.close()
        conn.close()

    return status, list_tour_json


def info_country_tour(id):
    status = True
    info_country_tour_json = []
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT city.name_city,
                                    date_.date_,
                                    tour.duration,
                                    tour_operator.name_operator
                            FROM tour
                            INNER JOIN city ON tour.city = city.id
                            INNER JOIN tour_operator ON tour.tour_operator = tour_operator.id
                            INNER JOIN tour_date ON tour.id = tour_date.id_tour
                            INNER JOIN date_ ON tour_date.id_date = date_.id
                            WHERE tour.country = {} 
                            GROUP BY (
                                        city.name_city,
                                        date_.date_,
                                        tour.duration,
                                        tour_operator.name_operator
                                      )
                            ORDER BY date_.date_
                            '''.format(
                                        id
                                        ))
            info_country = cur.fetchall()

        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        else:
            info_country_tour_json = [{
                                "city": tour[0],
                                "date": tour[1],
                                "duration": tour[2],
                                "operator": tour[3]
                                } for tour in info_country]

    cur.close()
    conn.close()
    return status, info_country_tour_json


def info_tour_func(id):
    status = True
    info_tour_json = {}
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''SELECT 
                                  tour.duration, 
                                  tour_operator.name_operator, 
                                  tour.description, 
                                  country.name_country, 
                                  city.name_city, 
                                  tour.id
                                  
                            FROM tour 
                            INNER JOIN country ON tour.country = country.id
                            INNER JOIN city ON tour.city = city.id
                            INNER JOIN tour_operator ON tour.tour_operator = tour_operator.id
                            
                            WHERE (tour.id = {})
                        '''.format(
                                    id
                                    ))
            '''
            meals.name_meals,
                                  hotel.name_hotel
                                  
            INNER JOIN tour_hotel ON tour.id = tour_hotel.id_tour
                            INNER JOIN hotel ON tour_hotel.id_hotel  = hotel.id
                            INNER JOIN meals_hotel ON hotel.id = meals_hotel.id_hotel
                            INNER JOIN meals ON meals_hotel.id_meals = meals.id'''
            tour = cur.fetchone()
            cur.execute('''
                            SELECT
                                    date_.date_
                            FROM date_
                            INNER JOIN tour_date ON tour_date.id_date = date_.id
                            INNER JOIN tour ON tour.id = tour_date.id_tour
                            WHERE (tour.id = {})
                            GROUP BY date_.date_
                        '''.format(
                                    id
                                    ))
            date_json = cur.fetchall()
            cur.execute('''
                            WITH hotel_on_tour AS (
                                                    SELECT 
                                                            tour_hotel.id_tour,
                                                            hotel.name_hotel,
                                                            hotel.id AS id
                                                    FROM tour_hotel
                                                    INNER JOIN hotel ON tour_hotel.id_hotel  = hotel.id
                                                    WHERE tour_hotel.id_tour = 16
                                                )
                            SELECT * 
                            FROM meals_hotel
                            INNER JOIN (SELECT * FROM hotel_on_tour) AS hotel_on_tour ON meals_hotel.id_hotel = hotel_on_tour.id
                            INNER JOIN meals ON meals_hotel.id_meals = meals.id
                        '''.format(
                                    id
                                    ))
            hotel_json = cur.fetchall()

            swap_meal = {}
            info_meal = {}
            for hotel_meal in hotel_json:
                try:
                    test = swap_meal[hotel_meal[0]]
                except KeyError:
                    swap_meal[hotel_meal[0]] = []
                info_meal[hotel_meal[0]] = {'name_meals': hotel_meal[7],
                                                'id_meals': hotel_meal[0],
                                                'hotels': ''}
                swap_meal[hotel_meal[0]].append({'name_hotels':hotel_meal[4],
                                             'id_hotels':hotel_meal[1],
                                             'price_day':hotel_meal[2]})


            for key in swap_meal.keys():
                info_meal[key]['hotels'] = swap_meal[key]

            #for meal in info_meal.values():
            #   print(meal)
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        else:
            info_tour_json = {
                "duration": tour[0],
                "operator": tour[1],
                "descr": tour[2],
                "country": tour[3],
                "city": tour[4],
                "id_tour": tour[5],
                "date": [str(date[0]) for date in date_json],
                "meals": [meal for meal in info_meal.values()]
                }
            print(info_tour_json)
        cur.close()
        conn.close()
    return status, info_tour_json


def all_tour_func():
    info_tour_json = []
    status = True
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            cur.execute(
                '''
                    SELECT 
                            tour.duration, 
                            tour_operator.name_operator, 
                            tour.description, 
                            country.name_country, 
                            city.name_city, 
                            tour.id,
                            date_.date_,
                            meals.name_meals,
                            hotel.name_hotel  
                    FROM tour 
                    INNER JOIN country ON tour.country = country.id
                    INNER JOIN city ON tour.city = city.id
                    INNER JOIN tour_operator ON tour.tour_operator = tour_operator.id
                    INNER JOIN tour_date ON tour.id = tour_date.id_tour
                    INNER JOIN date_ ON tour_date.id_date = date_.id
                    INNER JOIN tour_hotel ON tour.id = tour_hotel.id_tour
                    INNER JOIN hotel ON tour_hotel.id_hotel  = hotel.id
                    INNER JOIN meals_hotel ON hotel.id = meals_hotel.id_hotel
                    INNER JOIN meals ON meals_hotel.id_meals = meals.id
                    GROUP BY(
                                  tour.duration, 
                                  tour_operator.name_operator, 
                                  tour.description, 
                                  country.name_country, 
                                  city.name_city, 
                                  tour.id,
                                  date_.date_,
                                  meals.name_meals
                              )
                ''')
            all_tour = cur.fetchall()

        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        else:
            info_tour_json = [{
                "duration": tour[0],
                "operator": tour[1],
                "descript": tour[2],
                "name_country": tour[3],
                "name_city": tour[4],
                "id_tour": tour[5],
                "date": tour[6],
                "meals": tour[7],
                "hotel": tour[8]} for tour in all_tour]
        cur.close()
        conn.close()
    return status, info_tour_json


def edit_tour_func(id, di):
    conn = create_connection()
    status = True
    if conn:
        try:
            cur = conn.cursor()
            cur.execute('''UPDATE 
                                  tour
                            SET 
                                duration = '{}', 
                                tour_operator = (SELECT 
                                                        id 
                                                  FROM tour_operator 
                                                  WHERE name_operator = '{}'), 
                                description = '{}', 
                                country = (SELECT 
                                                id 
                                            FROM country 
                                            WHERE name_country = '{}'),
                                city = (SELECT 
                                              id 
                                        FROM city
                                        WHERE name_city = '{}')
                            WHERE (id = {})
                        '''.format(
                                    di['dur'][0],
                                    di['tourOp'][0],
                                    di['desc'][0],
                                    di['country'][0],
                                    di['city'][0],
                                    id
                                ))
            conn.commit()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status


def add_country_func(di):
    conn = create_connection()
    status = True
    if conn:
        cur = conn.cursor()
        try:
            key=di.keys()
            cur.execute('''
                            SELECT 
                                    id
                            FROM country
                            WHERE (name_country = '{}')
                        '''.format(
                                    di['count'][0]
                                    ))
            count = cur.fetchone()
            if count is None:
                cur.execute('''
                                SELECT 
                                        id
                                FROM country
                                ORDER BY id DESC 
                                limit 1
                            ''')
                id_country = cur.fetchone()[0] + 1
                cur.execute('''
                                INSERT INTO 
                                            country (
                                                      id, 
                                                      name_country
                                                      )
                                VALUES ({}, '{}')
                            '''.format(
                                        id_country,
                                        di['count'][0]
                                        ))
                conn.commit()
            else:
                id_country = count[0]
            for row in key:
                if row != 'count':
                    cur.execute('''
                                    SELECT *
                                    FROM city
                                    WHERE (name_city = '{}')
                                '''.format(
                                            di[row][0]
                                            ))
                    count = cur.fetchone()
                    if count is None:
                        cur.execute('''
                                        SELECT
                                                id
                                        FROM city
                                        ORDER BY id DESC 
                                        limit 1
                                    ''')
                        id_city = cur.fetchone()[0] + 1
                        cur.execute('''
                                        INSERT INTO 
                                                    city (
                                                          id, 
                                                          name_city, 
                                                          country
                                                          )
                                        VALUES ({}, '{}', {})
                                    '''.format(
                                                id_city,
                                                di[row][0],
                                                id_country
                                                ))
                        conn.commit()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status


def add_hotel_func(di):
    conn = create_connection()
    status = True

    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            INSERT INTO
                                        hotel (
                                                name_hotel, 
                                                rating, 
                                                description, 
                                                country, 
                                                city
                                              )
                            VALUES ('{}', {}, '{}', {}, {})
                        '''.format(
                                    di['hotel'][0],
                                    di['rait'][0],
                                    di['desc'][0],
                                    di['country'][0],
                                    di['city'][0]
                                ))
            conn.commit()

            key = di.keys()
            cur.execute('''
                            SELECT 
                                  id 
                            FROM hotel
                            WHERE (name_hotel = '{}')
                        '''.format(
                                    di['hotel'][0]
                                    ))
            id_hotel = cur.fetchone()[0]
            for row in key:
                if row.find('room') != -1:
                    count_room = str(row[0]) + 'count'
                    price_room = str(row[0]) + 'price_r'
                    cur.execute('''
                                    INSERT INTO 
                                                  room (
                                                        class_room, 
                                                        num_room,
                                                        hotel,
                                                        price_day
                                                        )
                                    VALUES ('{}', {}, {}, {})
                                '''.format(
                                            di[row][0],
                                            di[count_room][0],
                                            id_hotel,
                                            di[price_room][0]
                                            ))
                    conn.commit()
            for row in key:
                if row.find('meal') != -1:
                    price_meal = str(row[0]) + 'price_m'
                    cur.execute('''
                                    INSERT INTO 
                                                meals_hotel (
                                                              id_meals,
                                                              id_hotel,
                                                              price_day
                                                              )
                                    VALUES ({}, {}, {})
                                '''.format(
                                            di[row][0],
                                            id_hotel,
                                            di[price_meal][0]
                                            ))
                    conn.commit()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status


def info_city_func():
    conn = create_connection()
    #status = True
    info_city_json = []
    if conn:
        try:
            cur = conn.cursor()
            cur.execute('''
                            SELECT 
                                  id,
                                  name_city,
                                  country
                            FROM city
                            ORDER BY name_city
                        ''')
            info_city = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            #status = False
        else:
            info_city_json = [{
                "id_city": city[0],
                "name": city[1],
                "id_county" : city[2]} for city in info_city]
        cur.close()
        conn.close()
    return  info_city_json


def info_country_func():
    conn = create_connection()
    info_country_json = []
    #status = True
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT 
                                    id,
                                    name_country
                            FROM country
                            ORDER BY name_country
                        ''')
            info_country = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            #status = False
        else:
            info_country_json = [{
                "id": country[0],
                "name": country[1]} for country in info_country]
        cur.close()
        conn.close()
    return info_country_json


def info_meals_func():
    conn = create_connection()
    info_meals_json = []
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT 
                                    id,
                                    name_meals
                            FROM meals
                            ORDER BY name_meals
                        ''')
            info_meals = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
        else:
            info_meals_json = [{
                "id": meals[0],
                "name": meals[1]} for meals in info_meals]
        cur.close()
        conn.close()
    return info_meals_json


def info_class_room_func():
    conn = create_connection()
    info_class_room_json = []
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT 
                                    id,
                                    class
                            FROM class_room
                            ORDER BY class
                        ''')
            info_class_room = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
        else:
            info_class_room_json = [{
                "id": class_[0],
                "name": class_[1]} for class_ in info_class_room]
        cur.close()
        conn.close()
    return info_class_room_json


def info_tour_operator_func():
    conn = create_connection()
    info_tour_op_json = []
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT 
                                    id,
                                    name_operator
                            FROM tour_operator
                            ORDER BY name_operator
                        ''')
            info_tour_op = cur.fetchall()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
        else:
            info_tour_op_json = [{
                "id": tour_op[0],
                "name": tour_op[1]} for tour_op in info_tour_op]
        cur.close()
        conn.close()
    return info_tour_op_json


def filter_func(id, data):
    conn = create_connection()
    val = []
    filter_tour_json = []
    status = True
    if conn:
        cur = conn.cursor()
        try:

            if data['cityA'][0] != '':
                val.append(" city.name_city = '" + str(data['cityA'][0]) + "' ")
            if data['cityD'][0] != '':
                val.append("flight.city_departure = '" + str(data['cityD'][0]) + "' ")

            if data['date'][0] != '':
                val.append(" date_.date_ = '" + str(data['date'][0]) + "' ")

            if val == []:
                cur.execute('''
                               SELECT 
                                      city.name_city,
                                      date_.date_,
                                      tour.duration,
                                      tour_operator.name_operator
                               FROM tour
                               INNER JOIN city ON tour.city = city.id
                               INNER JOIN tour_operator ON tour.tour_operator = tour_operator.id
                               INNER JOIN tour_date ON tour.id = tour_date.id_tour
                               INNER JOIN date_ ON tour_date.id_date = date_.id
                               WHERE tour.country = {}
                               GROUP BY (
                                        city.name_city,
                                        date_.date_,
                                        tour.duration,
                                        tour_operator.name_operator
                                        )
                            '''.format(
                                        id
                                        ))
            else:
                cur.execute('''
                                SELECT 
                                      city.name_city,
                                      date_.date_,
                                      tour.duration,
                                      tour_operator.name_operator
                                FROM tour
                                INNER JOIN city ON tour.city = city.id
                                INNER JOIN tour_operator ON tour.tour_operator = tour_operator.id
                                INNER JOIN tour_date ON tour.id = tour_date.id_tour
                                INNER JOIN date_ ON tour_date.id_date = date_.id
                                WHERE tour.country = {} AND {}
                                GROUP BY (
                                          city.name_city,
                                          date_.date_,
                                          tour.duration,
                                          tour_operator.name_operator
                                          )
                                ORDER BY date_.date_
                          '''.format(
                                        id,
                                        "AND".join(val)
                                    ))

            filter_tour = cur.fetchall()

        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        else:

            filter_tour_json = [{
                                    "city": tour[0],
                                    "date": tour[1],
                                    "duration": tour[2],
                                    "operator": tour[3]
                                } for tour in filter_tour]

        cur.close()
        conn.close()
        print(filter_tour_json)
    return status, filter_tour_json


def info_user_func(id):
    conn = create_connection()
    status = True
    info_user_json = {}
    if conn:
        cur = conn.cursor()
        try:
            cur.execute('''
                            SELECT * 
                            FROM users
                            WHERE (id = '{}')
                        '''.format(
                                    id
                                    ))
            user = cur.fetchone()
            info_user_json = {
                'id': user[0],
                'name': user[1],
                'last_name': user[2],
                'login': user[3],
                'password': user[4],
                'email': user[5],
                'user_status': user[6],
                'phone': user[7],
                'session_key': user[8]}
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status, info_user_json


def delete_s_key(id):
    conn = create_connection()
    status = True
    if conn:
        cur = conn.cursor()
        try:

            cur.execute('''
                            UPDATE 
                                  users 
                            SET  
                                session_key = ''
                            WHERE (id = {})
                        '''.format(
                                    id
                                    ))
            conn.commit()
        except:
            logger.error(traceback.format_exc())
            traceback.print_exc()
            status = False
        cur.close()
        conn.close()
    return status
