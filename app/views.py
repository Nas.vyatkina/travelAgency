from flask import render_template, \
    request, make_response, redirect, url_for
from app import app
from app.config import create_connection
from app.work_db import login_db, registration_db, add_tour_func, info_user_func, select_tour_func, delete_s_key, \
    info_tour_func, edit_tour_func, all_tour_func, add_country_func, info_city_func, info_class_room_func, \
    info_country_func, info_meals_func, info_tour_operator_func, add_hotel_func, filter_func, info_country_tour


@app.route('/')
@app.route('/login')
def login():
    return render_template("login.html",
                           title='Login')


@app.route('/index')
def index():
    #id_user = request.cookies.get('id')
    #status_user, info_user = info_user_func(id_user)
    #status, rez = all_tour_func()
    #cities = info_city_func()
    #meals = info_meals_func()
    #data = {
    #        'cityD': [],
    #        'cityA': [],
    #        'date': [],
    #        'Dur': [],
    #        'countP': [],
    #        'priceMin': [],
    #        'priceMax': [],
    #        'meal': [],
    #        }
    status, country = select_tour_func()
    i = range(len(country))

    return render_template("index.html", country=country, range=i)


@app.route('/filter/<id>', methods=['POST'])
def filter(id):
    valuesFilter = dict(request.form)
    status, info_country_ = filter_func(id, valuesFilter)
    cities = info_city_func()

    return render_template('Sel_city.html', tour=info_country_, di=valuesFilter, cities=cities, id=id, range=range(len(info_country_)))



@app.route('/login_', methods=['POST'])
def login_():
    di = dict(request.form)
    data = {
        'cityD': [],
        'cityA': [],
        'date': [],
        'Dur': [],
        'countP': [],
        'priceMin': [],
        'priceMax': [],
        'meal': [],
        }
    status, info_user = login_db(di)
    if status:
        status, country = select_tour_func()
        i = range(len(country))

        resp = make_response(render_template("index.html", country=country, range=i))
        resp.set_cookie('id', str(info_user['id']))
        resp.set_cookie('key_s', info_user['session_key'])
        resp.set_cookie('name', info_user['name'])
        resp.set_cookie('last_name', info_user['last_name'])
        resp.set_cookie('email', info_user['email'])
        resp.set_cookie('status', str(info_user['user_status']))
        resp.set_cookie('phone', info_user['phone'])
        return resp
    else:
        return redirect(url_for('logout'))


@app.route('/reg')
def reg():
    return render_template("registration.html",
                           title='Registration')


@app.route('/registration', methods=['POST'])
def registration():
    di = dict(request.form)
    rez = registration_db(di)
    if rez == 0:
        return render_template("login.html", rez=rez)
    else:
        return render_template("registration.html", rez=rez)


@app.route('/tour/<id>', methods=['GET', 'POST'])
def tour(id):
    city = info_city_func()
    country = info_country_func()
    tour_op = info_tour_operator_func()
    if int(id) > -1:
        info_tour = info_tour_func(id)
        return render_template('Tour.html', id=int(id), info_tour=info_tour, city=city, country=country, tour_op=tour_op)
    return render_template('Tour.html', id=int(id), city=city, country=country, tour_op=tour_op)


@app.route('/sel_tour', methods=['GET', 'POST'])
def sel_tour():
    status, list_tour = select_tour_func()
    if status:
        return render_template('sel_tour.html', tours=list_tour)


@app.route('/editTour/<id>', methods=['POST'])
def edittour(id):
    di = dict(request.form)
    status = edit_tour_func(id, di)
    return render_template('edit_tour_finish.html', id=id, status=status)


@app.route('/addTour', methods=['POST'])
def addtour():
    di = dict(request.form)
    status = add_tour_func(di)
    if status:
        return redirect(url_for('tour', id=-2))


@app.route('/info_tour/<id>', methods=['GET'])
def infotour(id):
    status, info_tour = info_tour_func(id)
    if status:
        return render_template('info_tour.html', info_tour=info_tour)


@app.route('/country/<id>')
def country(id):
    return render_template('country.html', status=int(id))


@app.route('/info_country/<id>')
def info_country(id):
    data = {
                'cityD': [],
                'cityA': [],
                'date': [],
                }
    status,info_country_ = info_country_tour(id)
    cities = info_city_func()
    return render_template('Sel_city.html', tour=info_country_, di=data, cities=cities, id=id, range=range(len(info_country_)))


@app.route('/add_country', methods=['POST'])
def add_country():
    di = dict(request.form)
    status = add_country_func(di)
    if status:
        return redirect(url_for('country', id=0))
    return redirect(url_for('country', id=-1))


@app.route('/hotel/<id>')
def hotel(id):
    meals = info_meals_func()
    city = info_city_func()
    country = info_country_func()
    class_room = info_class_room_func()
    return render_template('hotel.html', meals=meals, city=city, country=country, class_room=class_room, id=int(id))


@app.route('/add_hotel', methods=['POST'])
def add_hotel():
    di = dict(request.form)
    #print(di)
    add_hotel_func(di)
    return redirect(url_for('hotel', id=-2))


@app.route('/order/<id>', methods=['GET', 'POST'])
def order(id):
    return render_template("order.html")


@app.route('/logout')
def logout():
    status = delete_s_key(request.cookies.get('id'))
    resp = redirect(url_for('login'))
    resp.set_cookie('id', "")
    resp.set_cookie('key_s', "")
    return resp


#def add_meals():
#    conn = create_connection()
#    if conn:
#        try:
#            cur = conn.cursor()
#            cur.execute('''Insert into meals ()''')